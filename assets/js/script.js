window.onload = function(){
  // for dropdown  
    var count=1;
    var dropdown = document.getElementsByClassName('dropin')[0];
    var dropfun = function (){
      console.log("fired")
      count++;
      var dropmenu = document.getElementsByClassName('drop')[0];
      if(count%2===0){
        dropmenu.classList.add('dropdown');
      }
      else{
        dropmenu.classList.remove('dropdown')
      }
      
    }
    dropdown.addEventListener('click',dropfun)
    dropdown.addEventListener('click',function(event){event.preventDefault()})


  //for hamburger 
  var noScroll= function(){
    window.scrollTo(0,0);
  }
  
  var hambur=document.getElementsByClassName('hambur')[0];
  var span=document.querySelectorAll('.hambur span');
  var counter=1;
  var clicked=function () {
    counter++;
    var showmenu=document.getElementsByClassName('menu')[0];
    if(counter%2===0){
      showmenu.classList.add('display');
      hambur.classList.add ('active');
      window.addEventListener('scroll', noScroll);
    }
    else{
      showmenu.classList.remove('display');
      hambur.classList.remove('active')
      window.removeEventListener('scroll', noScroll);
    }
  }
  hambur.addEventListener('click',clicked);

  //for slide to top {
    var gotoTop=document.getElementsByClassName("top")[0];
    var backTop = function(){
      window.scrollTo({
        top:0,
        left:0,
        behavior:"smooth"
      });
    }
    gotoTop.addEventListener('click',backTop)
    gotoTop.addEventListener('click',function(event){event.preventDefault()})
}

//for slider 
$(document).ready(function(){
  $('.sliders').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots:true
  });
})
